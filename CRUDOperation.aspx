﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CRUDOperation.aspx.cs" Inherits="CRUDOperationDemo.CRUDOperation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 186px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <th colspan="2">
                        <h2><b>Student Information</b></h2>
                    </th>
                </tr>
                <tr>
                    <td class="auto-style1">Student ID</td>
                    <td>
                        <asp:TextBox ID="txtid" placeholder="Enter Student ID" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="auto-style1">First Name</td>
                    <td>
                        <asp:TextBox ID="txtfname" placeholder="Enter Student First Name" runat="server"></asp:TextBox></td>
                </tr>
                 <tr>
                    <td class="auto-style1">Last Name</td>
                    <td>
                        <asp:TextBox ID="txtlname" placeholder="Enter Student Last Name" runat="server"></asp:TextBox></td>
                </tr>
                 <tr>
                    <td class="auto-style1">Date Of Birth</td>
                    <td>
                        <asp:TextBox ID="txtdob" TextMode="Date" placeholder="Select Student DOB" runat="server"></asp:TextBox></td>
                </tr>
                 <tr>
                    <td class="auto-style1">Address</td>
                    <td>
                        <asp:TextBox ID="txtaddr" TextMode="MultiLine" placeholder="Enter Full Address" runat="server" ></asp:TextBox></td>
                </tr>
                 <tr>
                    <td class="auto-style1">Mobile Number</td>
                    <td>
                        <asp:TextBox ID="txtmo" placeholder="Enter Mobile Number"  runat="server"></asp:TextBox></td>
                </tr>
                 <tr>
                    <td class="auto-style1">Email ID</td>
                    <td>
                        <asp:TextBox ID="txtemail" TextMode="Email" placeholder="Enter Email ID" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="auto-style1"></td>
                </tr>
                <tr>
                    <td class="auto-style1" >
                        <asp:Button ID="btnsub" runat="server" Text="Insert" OnClick="btnsub_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnupd" runat="server" Text="Update" OnClick="btnupd_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btndel" runat="server" Text="Delete" OnClick="btndel_Click" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="lblmsg" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><asp:GridView ID="gv1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
                        <AlternatingRowStyle BackColor="White" />
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                        </asp:GridView></td>
                </tr>
                    
                </table>
        </div>
    </form>
</body>
</html>
