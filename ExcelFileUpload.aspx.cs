﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.OleDb;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Data.Common;

namespace CRUDOperationDemo
{
    public partial class ExcelFileUpload : System.Web.UI.Page
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString.ToString());

        protected void Page_Load(object sender, EventArgs e)
        {
            BindGridView();
        }

        protected void btnupl_Click(object sender, EventArgs e)
        {
            if (Fileupload1.PostedFile != null)
            {
                try
                {
                    string path = string.Concat(Server.MapPath("~/UploadFile/" + Fileupload1.FileName));
                    Fileupload1.SaveAs(path);
                    string excelCon = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);
                    using (OleDbConnection connection = new OleDbConnection(excelCon))
                    {
                        OleDbCommand cmd = new OleDbCommand("select * from [Sheet1$]", connection);
                        connection.Open();
                        DbDataReader dr = cmd.ExecuteReader(); 
                        SqlBulkCopy bulkInsert = new SqlBulkCopy(con);
                        bulkInsert.DestinationTableName = "ExcelFileData";
                        bulkInsert.WriteToServer(dr);
                        BindGridView();
                        lblmsg.Text = "Your file uploaded successfully";
                    }
                }
                catch (Exception)
                {
                    lblmsg.Text = "Your file not uploaded";
                }
            }
        }
        protected void BindGridView()
        {
            SqlCommand cmd = new SqlCommand("spGetExcelFile", con);
            cmd.CommandType = CommandType.StoredProcedure;
            con.Open();
            gvexcelfile.DataSource = cmd.ExecuteReader();
            gvexcelfile.DataBind();
            con.Close();
        }
    }
}


/*
 Stored Procedure For GetExcelFile

create proc spGetExcelFile
as
begin
select * from ExcelFileData
end

 */
