﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CRUDOperationDemo
{
    public partial class CRUDOperation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnsub_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings[" "].ConnectionString.ToString());
                SqlCommand cmd = new SqlCommand("Insert_Record", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Student_Id", SqlDbType.Int).Value = txtid.Text;
                cmd.Parameters.AddWithValue("@First_Name", SqlDbType.VarChar).Value = txtfname.Text;
                cmd.Parameters.AddWithValue("@Last_Name", SqlDbType.VarChar).Value = txtlname.Text;
                cmd.Parameters.AddWithValue("@Date_Of_Birth", SqlDbType.Date).Value = txtdob.Text;
                cmd.Parameters.AddWithValue("@Address", SqlDbType.VarChar).Value = txtaddr.Text;
                cmd.Parameters.AddWithValue("@Mobile_Number", SqlDbType.VarChar).Value = txtmo.Text;
                cmd.Parameters.AddWithValue("@Email_Id", SqlDbType.VarChar).Value = txtemail.Text;
                con.Open();
                int k = cmd.ExecuteNonQuery();
                if (k != 0)
                {
                    lblmsg.Text = "Your Record Is Insert Successfully...!";
                }
                con.Close();
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
            }
            finally
            {
                DisplayData();
            }         
        }

        protected void btnupd_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings[" "].ConnectionString.ToString());
                SqlCommand cmd = new SqlCommand("Update_Record", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Student_Id", SqlDbType.Int).Value = txtid.Text;
                con.Open();
                int k = cmd.ExecuteNonQuery();
                if (k != 0)
                {
                    lblmsg.Text = "Your Record Is Update Successfully...!";
                }
                con.Close();
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
            }
            finally
            {
                DisplayData();
            }
        }

        protected void btndel_Click(object sender, EventArgs e)
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings[" "].ConnectionString.ToString());
                SqlCommand cmd = new SqlCommand("Delete_Record", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Studen_Id", SqlDbType.Int).Value = txtid.Text;
                con.Open();
                int k = cmd.ExecuteNonQuery();
                {
                    lblmsg.Text = "Your Record Is Delete Successfully...!";
                }
                con.Close();
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
            }
            finally
            {
                DisplayData();
            }
        }

        public void DisplayData()
        {
            try
            {
                SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings[" "].ConnectionString.ToString());
                SqlCommand cmd = new SqlCommand("Select_Record", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                DataTable dt = new DataTable();
                SqlDataAdapter sda = new SqlDataAdapter();
                sda.SelectCommand = cmd;
                sda.Fill(dt);
                gv1.DataSource = dt;
                gv1.DataBind();
                con.Close();
            }
            catch (Exception ex)
            {
                lblmsg.Text = ex.Message;
            }

        }
    }
}

/* 
--Stored Procedure For Insert Record--

create proc Insert_Record
@Student_Id int,
@First_Name nvarchar(50),
@Last_Name nvarchar(50),
@Date_Of_Birth date,
@Address nvarchar(max),
@Mobile_Number nvarchar(50),
@Email_Id nvarchar(100)
as
begin
insert into Student_Information (Student_Id, First_Name, Last_Name, Date_Of_Birth, Address, Mobile_Number, Email_Id)
            values              (@Student_Id, @First_Name, @Last_Name, @Date_Of_Birth, @Address, @Mobile_Number, @Email_Id)
end

--Stored Procedure For Update Record--

create proc Update_Record
@Student_Id int
as
begin
update	 Student_Information
set   	 First_Name	    = @First_Name,
         Last_Name      = @Last_Name,
         Date_Of_Birth  = @Date_Of_Birth,
         Address        = @Address,
         Mobile_Number  = @Mobile_Number,
         Email_Id       = @Email_Id
where	 Student_Id	    = @Student_Id
end

--Stored Procedure For Delete Record--

create proc Delete_Record
@Student_Id int
as
begin
delete from Student_Information where Student_Id = @Student_Id
end

--Stored Procedure For Display Record--

create proc Select_Record
as
begin
select * from Student_Information
end

*/
